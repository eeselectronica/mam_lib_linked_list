/*
* mam_linked_list.c
*
* Created: 28/01/2022 02:37:52 p. m.
*  Author: Ing. Ramses Garcia Dilone
*/
#include "mam_linked_list.h"
#include "mam_log.h"
#include <string.h>



void mam_list_init(mam_list_t *me)
{
	me->head = NULL;
	me->size = 0;
	
}

uint8_t mam_list_is_empty(mam_list_t *me)
{
	return (me->head == NULL);
}

void mam_list_insert_front(mam_list_t *me, uint32_t key, uint32_t value)
{
	mam_node_t *aux_node = mam_list_find_node(me,key);
	if(aux_node != NULL)
	{
		aux_node->value = value;
	}
	else
	{						   		
		mam_node_t *new_node = (mam_node_t*)malloc(sizeof(mam_node_t));
		if (new_node != NULL)
		{
			me->size++;
			if (mam_list_is_empty(me))
			{
				new_node->key=key;
				new_node->value = value;
				new_node->next = NULL;
				me->head = new_node;
			}
			else
			{
				new_node->key = key;
				new_node->value = value;
				new_node->next = me->head;
				me->head = new_node;
			}
		}
	}
}

void mam_list_remove_node(mam_list_t * me, uint32_t key)
{
	if(!mam_list_is_empty(me))
	{
		mam_node_t *current_node = me->head;
		mam_node_t *prev_node = NULL;
		while(current_node->key != key)
		{
			if(current_node->next == NULL)
			{
				return;
			}
			else
			{
				prev_node = current_node;
				current_node = current_node->next;
			}
		}
		
		if(current_node == me->head)
		{
			me->head = me->head->next;
		}
		else
		{
			prev_node->next = current_node->next;
		}
		me->size--;
		free(current_node);
		
	}
}

void mam_list_print(mam_list_t *me)
{
	mam_node_t  *node_aux = me->head;
	
	if(node_aux == NULL)
	{
		MAM_PRINTF(LOG_DBG,"LIST IS EMPTY");
	}
	else
	{
		while(node_aux != NULL)
		{
			MAM_PRINTF(LOG_DBG,"[%x , %d]\r\n",node_aux->key,node_aux->value);
			node_aux = node_aux->next;
		}
	}
	
}

mam_node_t *  mam_list_find_node(mam_list_t *me, uint32_t key)
{

	if(mam_list_is_empty(me))
	return NULL;
	mam_node_t *mam_current = me->head;

	while (mam_current->key != key)
	{
		if (mam_current->next == NULL)
		{
			return NULL;
		}
		else
		{
			mam_current = mam_current->next;
		}
	}
	return mam_current;
}

uint32_t mam_linked_list_get_node_val_and_del(mam_list_t* me, uint32_t key)
{
	if(mam_list_is_empty(me))
	return;
	
	mam_node_t *current = mam_list_find_node(me,key);
	uint32_t temp = current->value;
	mam_list_remove_node(me, key);
	return temp;
}

void mam_list_clean(mam_list_t * me)
{
	if (me == NULL) return NULL;
	mam_node_t *mam_current = me->head;
	mam_node_t *_next;
	while (mam_current != NULL)
	{
		_next = mam_current->next;
		free(mam_current);
		mam_current = _next;
	}
	me->head= NULL;
}
