/*
* mam_linked_list.h
*
* Created: 28/01/2022 02:38:12 p. m.
*  Author: Ing. Ramses Garcia Dilone
*/


#ifndef MAM_LINKED_LIST_H_
#define MAM_LINKED_LIST_H_





#ifdef __cplusplus
extern "C"
{
	#endif /* __cplusplus */

	#include "stdint.h"
	#include "stdio.h"

	#pragma pack (1)
	typedef struct mam_node_t
	{
		uint32_t key;
		uint32_t value;
		struct mam_node_t *next;
	}mam_node_t;

	typedef struct mam_list_t
	{
		uint32_t size;
		mam_node_t *head;
	}mam_list_t;
	

	void mam_list_init(mam_list_t *me);
	void mam_list_print(mam_list_t *me);
	void mam_list_clean(mam_list_t * me);
	uint8_t mam_list_is_empty(mam_list_t *me);
	void mam_list_remove_node(mam_list_t * me, uint32_t key);
	mam_node_t *mam_list_find_node(mam_list_t *me, uint32_t key);
	void mam_list_insert_front(mam_list_t *me, uint32_t key, uint32_t value);
	uint32_t mam_linked_list_get_node_val_and_del(mam_list_t* me, uint32_t key);
	#ifdef __cplusplus
}
#endif

#endif /* MAM_LINKED_LIST_H_ */